const inputNota = document.querySelector('#nota');
const botaoAdicionar = document.querySelector('#adicionar');
const boxTexto = document.querySelector('.box-medias');
const botaoCalcular = document.querySelector('#calcular');
const mediaFinal = document.querySelector('#media');

var notas = [];
var quantidadeNota = 0;

botaoAdicionar.addEventListener('click', () => {
  if (inputNota.value == '') {

    alert('Por favor, insira uma nota.');
  } else if (isNaN(inputNota.value) || inputNota.value < 0 || inputNota.value > 10) {

    alert('A nota digitada é inválida. Por favor, insira uma nota válida.')
    inputNota.value = '';
  } else {

    notas.push(inputNota.value);
    quantidadeNota++;
    boxTexto.textContent += `A nota ${quantidadeNota} foi ${inputNota.value}\n`;
    inputNota.value = '';
  }
});

botaoCalcular.addEventListener('click', () => {
  let somaNotas = 0;
  
  notas.forEach(nota => {
    somaNotas += parseFloat(nota);
  })
  mediaFinal.innerText = (somaNotas / notas.length).toFixed(2);
});